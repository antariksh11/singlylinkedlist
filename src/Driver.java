import java.util.List;

public class Driver {

	public static void main(String[] args) {
		List<String> list = new SinglyLinkedList<String>();
		
		System.out.println(list);
		System.out.print("Is Empty : " + list.isEmpty() + "\n");
		
		list.add("Apples");
		list.add("Oranges");
		list.add("Bananas");
		
		System.out.println(list + "size: " + list.size() + "\n");
		
		list.add(0, "Mango");

		System.out.println(list + "size: " + list.size() + "\n");
		
		list.add(2, "Pineapple");

		System.out.println(list + "size: " + list.size() + "\n");
		
		list.add(4, "Kiwi");

		System.out.println(list + "size: " + list.size() + "\n");
		
		list.set(4, "Passion Fruit");

		System.out.println(list + "size: " + list.size() + "\n");
		
		list.remove(0);

		System.out.println(list + "size: " + list.size() + "\n");
		
		list.remove(2);

		System.out.println(list + "size: " + list.size() + "\n");
		
		list.remove("Passion Fruit");

		System.out.println(list + "size: " + list.size() + "\n");
		
		System.out.print(list.get(2) + "\n");
		
		System.out.print(list.contains("Apples") + "\n");
		System.out.print(list.contains("Passion Fruit") + "\n");
		

		System.out.print("Is Empty : " + list.isEmpty() + "\n");
		
	}

}
