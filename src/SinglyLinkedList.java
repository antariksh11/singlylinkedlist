import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class SinglyLinkedList<E> implements List<E> {

	private int size;
	private Node<E> head;
	private Node<E> tail;
	
	public SinglyLinkedList() {}
	
	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public boolean contains(Object o) {
		if (isEmpty()) {
			return false;
		} else {
			Node<E> currentNode = head;
			while (currentNode.getNext() != null) {
				if (o != null && o.equals(currentNode.getData())) {
					return true;
				} else if (o == null && currentNode.getData() == null) {
					return true;
				} else {
					currentNode = currentNode.getNext();
				}
			}
		}
		
		return false;
	}

	@Override
	public Object[] toArray() {
		
		if (isEmpty()) {
			return null;
		}
		
		Object[] array = new Object[size];
		
		Node<E> currentNode = head;
		int i = 0;
		
		while (currentNode.getNext() != null) {
			array[i] = currentNode.getData();
			
			currentNode = currentNode.getNext();
			i++;
		}
		
		return array;
	}

	@Override
	public boolean add(E e) {
		
		Node<E> newNode = new Node<E>(e);
		
		if (isEmpty()) {
			head = newNode;
			tail = newNode;
		} else {
			tail.setNext(newNode);
			tail = newNode;
		}
		
		size++;
		
		return true;
	}

	@Override
	public boolean remove(Object o) {
		
		if (isEmpty()) {
			return false;
		}
		
		boolean didRemove = false;
		
		Node<E> prevNode = head;
		Node<E> currentNode = head;
		
		while (currentNode != null) {
			
			if (areEqual(o, currentNode)) {
				removeObjectHelper(currentNode, prevNode);
				didRemove = true;
			}
			
			prevNode = currentNode;
			currentNode = currentNode.getNext();
		}
		
		if (didRemove) {
			size--;
		}
		
		return didRemove;
	}
	
	private boolean areEqual(Object o, Node<E> node) {
		return ((o != null && o.equals(node.getData())) || (o == null && node.getData() == null));
	}
	
	private void removeObjectHelper(Node<E> currentNode, Node<E> prevNode) {
		if (currentNode.equals(head)) {
			head = head.getNext();
			
			if (head == null) {
				tail = null;
			}
		} else {
			prevNode.setNext(currentNode.getNext());
			
			if (currentNode.equals(tail)) {
				tail = prevNode;
			}
		}
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		
		for (E data : c) {
			if (!add(data)) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public void clear() {
		head = null;
		tail = null;
		size = 0;
	}

	@Override
	public E get(int index) {
		
		if (index >= size) {
			throw new IndexOutOfBoundsException("Specified index is out of bounds.");
		}
		
		Node<E> currentNode = head;
		int i = 0;
		
		while (i < index) {
			currentNode = currentNode.getNext();
			i++;
		}
		
		return currentNode.getData();
	}

	@Override
	public E set(int index, E element) {
		
		if (index >= size) {
			throw new IndexOutOfBoundsException("Specified index is out of bounds.");
		}
		
		Node<E> currentNode = head;
		int i = 0;
		
		while (i < index) {
			currentNode = currentNode.getNext();
			i++;
		}
		
		E data = currentNode.getData();
		
		currentNode.setData(element);
		
		return data;
	}

	@Override
	public void add(int index, E element) {
		
		if (index >= size) {
			throw new IndexOutOfBoundsException("Specified index is out of bounds.");
		}
		
		Node<E> currentNode = head;
		Node<E> prevNode = head;
		
		int i = 0;
		
		while (i < index) {
			prevNode = currentNode;
			currentNode = currentNode.getNext();
			
			i++;
		}
		
		Node<E> newNode = new Node<E>(element);
		newNode.setNext(currentNode);
		
		if (currentNode.equals(head)) {
			head = newNode;
		} else {
			prevNode.setNext(newNode);
		}
		
		size++;
	}

	@Override
	public E remove(int index) {
		
		if (index >= size) {
			throw new IndexOutOfBoundsException("Specified index is out of bounds.");
		}
		
		E dataRemoved = null;
		
		Node<E> currentNode = head;
		Node<E> prevNode = head;
		int i = 0;
		
		while (i < index) {
			
			prevNode = currentNode;
			currentNode = currentNode.getNext();
			
			i++;
		}
		
		dataRemoved = currentNode.getData();
		
		if (currentNode.equals(head)) {
			head = head.getNext();
			
			if (currentNode.equals(tail)) {
				tail = null;
			}
		} else {
			prevNode.setNext(currentNode.getNext());
			
			if (currentNode.equals(tail)) {
				tail = prevNode;
			}
		}
		
		size--;
		
		return dataRemoved;
	}

	@Override
	public int indexOf(Object o) {
		
		Node<E> currentNode = head;
		int i = 0;
		
		while(currentNode != null) {
			if (areEqual(o, currentNode)) {
				return i;
			} else {
				i++;
				currentNode = currentNode.getNext();
			}
		}
		
		return -1;
	}

	@Override
	public int lastIndexOf(Object o) {
		
		Node<E> currentNode = head;
		int i = 0;
		boolean foundValue = false;
		
		while (currentNode != null) {
			if (areEqual(o, currentNode)) {
				foundValue = true;
			} else {
				currentNode = currentNode.getNext();
				i++;
			}
		}
		
		if (!foundValue) {
			return -1;
		} else {
			return i;
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		Node<E> currentNode = head;
		
		while (currentNode != null) {
			builder.append(String.format("%s\n", currentNode.getData().toString()));
			
			currentNode = currentNode.getNext();
		}
		
		return builder.toString();
	}
	
	
	
	// ------------------------------------------------------
	// ------------------------------------------------------
	// The below are methods that we don't care to implement.
	// ------------------------------------------------------
	// ------------------------------------------------------
	
	@Override
	public Iterator<E> iterator() {
		return null;
	}
	
	@Override
	public <T> T[] toArray(T[] a) {
		return null;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return false;
	}
	
	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return false;
	}
	
	@Override
	public ListIterator<E> listIterator() {
		return null;
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		return null;
	}

	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		return null;
	}
}
